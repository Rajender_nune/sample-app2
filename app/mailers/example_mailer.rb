class ExampleMailer < ApplicationMailer
     default from: ""
  def sample_email(post)
    @post = post
    mail(to: @post.title, subject: 'Sample Email')
  end

end
